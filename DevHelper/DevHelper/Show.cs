﻿using DevHelper.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace DevHelper
{
    public static class Show
    {
        public enum Entity
        {
            Git,
            Dotnet,
            Html,
            Css,
            Sql
        }

        public static void ShowEntity(Entity entity)
        {
            switch (entity)
            {
                case Entity.Git:
                    ShowGit();
                    break;
                case Entity.Dotnet:
                    ShowDotnet();
                    break;
                case Entity.Html:
                    ShowHtml();
                    break;
                case Entity.Css:
                    ShowCss();
                    break;
                case Entity.Sql:
                    ShowSql();
                    break;
                default:
                    Console.WriteLine("Oops. Server error.");
                    break;
            }
        }

        private static void ShowGit()
        {
            try
            {
                var gits = Queries.GetGit();
                Console.WriteLine("Git:");
                foreach (var item in gits)
                {
                    Console.WriteLine($"{item.id}) {item.code}");
                    Console.WriteLine();
                }
                Console.WriteLine();
            }
            catch (Exception e)
            {
                Console.WriteLine("Oops. Server error.");
            }
        }

        private static void ShowDotnet()
        {
            try
            {
                var dotnets = Queries.GetDotnet();
                Console.WriteLine(".NET:");
                foreach (var item in dotnets)
                {
                    Console.WriteLine($"{item.id}) {item.code}");
                    Console.WriteLine();
                }
                Console.WriteLine();
            }
            catch (Exception)
            {
                Console.WriteLine("Oops. Server error.");
            }
        }

        private static void ShowHtml()
        {
            try
            {
                var htmls = Queries.GetHtml();
                Console.WriteLine("Html:");
                foreach (var item in htmls)
                {
                    Console.WriteLine($"{item.id}) {item.code}");
                    Console.WriteLine();
                }
                Console.WriteLine();
            }
            catch (Exception)
            {
                Console.WriteLine("Oops. Server error.");
            }
        }

        private static void ShowSql()
        {
            try
            {
                var sqls = Queries.GetSql();
                Console.WriteLine("Sql:");
                foreach (var item in sqls)
                {
                    Console.WriteLine($"{item.id}) {item.code}");
                    Console.WriteLine();
                }
                Console.WriteLine();
            }
            catch (Exception)
            {
                Console.WriteLine("Oops. Server error.");
            }
        }

        private static void ShowCss()
        {
            try
            {
                var csss = Queries.GetCss();
                Console.WriteLine("Css:");
                foreach (var item in csss)
                {
                    Console.WriteLine($"{item.id}) {item.code}");
                    Console.WriteLine();
                }
            }
            catch (Exception)
            {
                Console.WriteLine("Oops. Server error.");
            }
            Console.WriteLine();
        }
    }
}
