﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevHelper.Entity
{
    internal static class Drop
    {
        public enum Entity
        {
            Git,
            Dotnet,
            Html,
            Css,
            Sql
        }

        public static void DropEntity(Entity entity)
        {
            switch (entity)
            {
                case Entity.Git:
                    DropGit();
                    break;
                case Entity.Dotnet:
                    DropDotnet();
                    break;
                case Entity.Html:
                    DropHtml();
                    break;
                case Entity.Css:
                    DropCss();
                    break;
                case Entity.Sql:
                    DropSql();
                    break;
                default:
                    Console.WriteLine("Oops. Server error.");
                    break;
            }
        }

        private static void DropGit()
        {
            Console.Write("Drop code id: ");
            string stringId = Console.ReadLine();

            bool result = int.TryParse(stringId, out int id);
            if(result is false)
            {
                Console.WriteLine("Wrong id");
                return;
            }

            try
            {
               bool answer = Queries.DropGit(id);

                if(answer == true )
                {
                    Console.Clear();
                    Logic.Git();
                }
                else
                {
                    Console.WriteLine("Wrong id");
                }
            }
            catch (Exception)
            {
                Console.WriteLine("Oops. Server error.");
            }
        }

        private static void DropHtml()
        {
            Console.Write("Drop code id: ");
            string stringId = Console.ReadLine();

            bool result = int.TryParse(stringId, out int id);
            if (result is false)
            {
                Console.WriteLine("Wrong id");
                return;
            }

            try
            {
                bool answer = Queries.DropHtml(id);

                if (answer == true)
                {
                    Console.Clear();
                    Logic.Html();
                }
                else
                {
                    Console.WriteLine("Wrong id");
                }
            }
            catch (Exception)
            {
                Console.WriteLine("Oops. Server error.");
            }
        }

        private static void DropCss()
        {
            Console.Write("Drop code id: ");
            string stringId = Console.ReadLine();

            bool result = int.TryParse(stringId, out int id);
            if (result is false)
            {
                Console.WriteLine("Wrong id");
                return;
            }

            try
            {
                bool answer = Queries.DropCss(id);

                if (answer == true)
                {
                    Console.Clear();
                    Logic.Css();
                }
                else
                {
                    Console.WriteLine("Wrong id");
                }
            }
            catch (Exception)
            {
                Console.WriteLine("Oops. Server error.");
            }
        }

        private static void DropDotnet()
        {
            Console.Write("Drop code id: ");
            string stringId = Console.ReadLine();

            bool result = int.TryParse(stringId, out int id);
            if (result is false)
            {
                Console.WriteLine("Wrong id");
                return;
            }

            try
            {
                bool answer = Queries.DropDotnet(id);

                if (answer == true)
                {
                    Console.Clear();
                    Logic.DotNet();
                }
                else
                {
                    Console.WriteLine("Wrong id");
                }
            }
            catch (Exception)
            {
                Console.WriteLine("Oops. Server error.");
            }
        }

        private static void DropSql()
        {
            Console.Write("Drop code id: ");
            string stringId = Console.ReadLine();

            bool result = int.TryParse(stringId, out int id);
            if (result is false)
            {
                Console.WriteLine("Wrong id");
                return;
            }

            try
            {
                bool answer = Queries.DropSql(id);

                if (answer == true)
                {
                    Console.Clear();
                    Logic.Sql();
                }
                else
                {
                    Console.WriteLine("Wrong id");
                }
            }
            catch (Exception)
            {
                Console.WriteLine("Oops. Server error.");
            }
        }
    }
}
