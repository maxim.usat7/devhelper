﻿using DevHelper.Entity.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DevHelper.Entity
{
    internal class Database : DbContext
    {
        public DbSet<Dotnet> dotnets { get; set; }
        public DbSet<Git> gits { get; set; }
        public DbSet<Html> htmls { get; set; }
        public DbSet<Css> csss { get; set; }
        public DbSet<Sql> sqls { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder builder)
        {
            builder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=devhelper;Trusted_Connection=True;");
        }
    }
}
