﻿using System;
using System.Collections.Generic;
using System.Text;
using DevHelper.Entity.Models;

namespace DevHelper.Entity
{
    internal static class Add
    {
        public enum Entity
        {
            Git,
            Dotnet,
            Html,
            Css,
            Sql
        }

        public static void AddEntity(Entity entity)
        {
            switch (entity)
            {
                case Entity.Git:
                    AddGit();
                    break;
                case Entity.Dotnet:
                    AddDotnet();
                    break;
                case Entity.Html:
                    AddHtml();
                    break;
                case Entity.Css:
                    AddCss();
                    break;
                case Entity.Sql:
                    AddSql();
                    break;
                default:
                    Console.WriteLine("Oops. Server error.");
                    break;
            }
        }

        private static void AddGit()
        {
            try
            {
                Console.Write("Code: ");
                string code = Console.ReadLine();
                Queries.AddGit(new Git(code));
            }
            catch (Exception)
            {
                Console.WriteLine("Oops. Server error.");
            }
        }

        private static void AddDotnet()
        {
            try
            {
                Console.Write("Code: ");
                string code = Console.ReadLine();
                Queries.AddDotnet(new Dotnet(code));
            }
            catch (Exception)
            {
                Console.WriteLine("Oops. Server error.");
            }
        }

        private static void AddHtml()
        {
            try
            {
                Console.Write("Code: ");
                string code = Console.ReadLine();
                Queries.AddHtml(new Html(code));
            }
            catch (Exception)
            {
                Console.WriteLine("Oops. Server error.");
            }
        }

        private static void AddSql()
        {
            try
            {
                Console.Write("Code: ");
                string code = Console.ReadLine();
                Queries.AddSql(new Sql(code));
            }
            catch (Exception)
            {
                Console.WriteLine("Oops. Server error.");
            }
        }

        private static void AddCss()
        {
            try
            {
                Console.Write("Code: ");
                string code = Console.ReadLine();
                Queries.AddCss(new Css(code));
            }
            catch (Exception)
            {
                Console.WriteLine("Oops. Server error.");
            }
            Console.WriteLine();
        }
    }
}
