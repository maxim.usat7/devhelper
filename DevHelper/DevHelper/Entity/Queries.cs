﻿using DevHelper.Entity.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace DevHelper.Entity
{
    public static class Queries
    {
        public static IList<Git> GetGit()
        {
            using (Database db = new Database())
            {
                var result = db.gits.ToList();
                return result;
            }
        }

        public static IList<Dotnet> GetDotnet()
        {
            using (Database db = new Database())
            {
                var result = db.dotnets.ToList();
                return result;
            }
        }

        public static IList<Html> GetHtml()
        {
            using (Database db = new Database())
            {
                var result = db.htmls.ToList();
                return result;
            }
        }

        public static IList<Sql> GetSql()
        {
            using (Database db = new Database())
            {
                var result = db.sqls.ToList();
                return result;
            }
        }

        public static IList<Css> GetCss()
        {
            using (Database db = new Database())
            {
                var result = db.csss.ToList();
                return result;
            }
        }

        public static void AddGit(Git git)
        {
            using (Database db = new Database())
            {
                db.gits.Add(git);
                db.SaveChanges();
            }
        }

        public static void AddDotnet(Dotnet dotnet)
        {
            using (Database db = new Database())
            {
                db.dotnets.Add(dotnet);
                db.SaveChanges();

            }
        }

        public static void AddHtml(Html html)
        {
            using (Database db = new Database())
            {
                db.htmls.Add(html);
                db.SaveChanges();
            }
        }

        public static void AddSql(Sql sql)
        {
            using (Database db = new Database())
            {
                db.sqls.Add(sql);
                db.SaveChanges();
            }
        }

        public static void AddCss(Css css)
        {
            using (Database db = new Database())
            {
                db.csss.Add(css);
                db.SaveChanges();
            }
        }

        public static bool DropGit(int id)
        {
            using (Database db = new Database())
            {
                Git git = db.gits.FirstOrDefault(dbGit => dbGit.id == id);

                if (git is null)
                {
                    return false;
                }
                else
                {
                    db.gits.Remove(git);
                    db.SaveChanges();
                    return true;
                }
            }
        }

        public static bool DropHtml(int id)
        {
            using (Database db = new Database())
            {
                Html html = db.htmls.FirstOrDefault(dbHtml => dbHtml.id == id);

                if (html is null)
                {
                    return false;
                }
                else
                {
                    db.htmls.Remove(html);
                    db.SaveChanges();
                    return true;
                }
            }
        }

        public static bool DropDotnet(int id)
        {
            using (Database db = new Database())
            {
                Dotnet dotnet = db.dotnets.FirstOrDefault(dbDotnet => dbDotnet.id == id);

                if (dotnet is null)
                {
                    return false;
                }
                else
                {
                    db.dotnets.Remove(dotnet);
                    db.SaveChanges();
                    return true;
                }
            }
        }

        public static bool DropSql(int id)
        {
            using (Database db = new Database())
            {
                Sql sql = db.sqls.FirstOrDefault(dbSql => dbSql.id == id);

                if (sql is null)
                {
                    return false;
                }
                else
                {
                    db.sqls.Remove(sql);
                    db.SaveChanges();
                    return true;
                }
            }
        }

        public static bool DropCss(int id)
        {
            using (Database db = new Database())
            {
                Css css = db.csss.FirstOrDefault(dbCss => dbCss.id == id);

                if (css is null)
                {
                    return false;
                }
                else
                {
                    db.csss.Remove(css);
                    db.SaveChanges();
                    return true;
                }
            }
        }
    }
}
