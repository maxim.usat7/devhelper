﻿using DevHelper.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace DevHelper
{
    public static class Logic
    {
        public static void Language()
        {
            while (true)
            {
                Console.WriteLine("[dotnet] [git] [html] [css] [sql]");
                Console.Write("Command: ");
                string command = Console.ReadLine();

                switch (command)
                {
                    case "git":
                        Git();
                        break;
                    case "dotnet":
                        DotNet();
                        break;
                    case "html":
                        Html();
                        break;
                    case "css":
                        Css();
                        break;
                    case "sql":
                        Sql();
                        break;
                    default:
                        Console.WriteLine("Wrong command");
                        break;
                }
            }
        }

        public static void Git()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("[return] [show] [add] [drop]");

            while (true)
            {
                Console.Write("Command: ");
                string command = Console.ReadLine();

                switch (command)
                {
                    case "drop":
                        Drop.DropEntity(Drop.Entity.Git);
                        break;
                    case "return":
                        Console.Clear();
                        Console.ResetColor();
                        return;
                    case "show":
                        Show.ShowEntity(Show.Entity.Git);
                        break;
                    case "add":
                        Add.AddEntity(Add.Entity.Git);
                        break;
                    default:
                        Console.WriteLine("Wrong command");
                        break;
                }
            }
        }

        public static void DotNet()
        {
            Console.WriteLine("[return] [show] [add] [drop]");

            while (true)
            {
                Console.ForegroundColor = ConsoleColor.Magenta;
                Console.Write("Command: ");
                string command = Console.ReadLine();

                switch (command)
                {
                    case "drop":
                        Drop.DropEntity(Drop.Entity.Dotnet);
                        break;
                    case "return":
                        Console.Clear();
                        Console.ResetColor();
                        return;
                    case "show":
                        Show.ShowEntity(Show.Entity.Dotnet);
                        break;
                    case "add":
                        Add.AddEntity(Add.Entity.Dotnet);
                        break;
                    default:
                        Console.WriteLine("Wrong command");
                        break;
                }
            }
        }

        public static void Html()
        {
            Console.WriteLine("[return] [show] [add] [drop]");

            while (true)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.Write("Command: ");
                string command = Console.ReadLine();

                switch (command)
                {
                    case "drop":
                        Drop.DropEntity(Drop.Entity.Html);
                        break;
                    case "return":
                        Console.ResetColor();
                        Console.Clear();
                        return;
                    case "show":
                        Show.ShowEntity(Show.Entity.Html);
                        break;
                    case "add":
                        Add.AddEntity(Add.Entity.Html);
                        break;
                    default:
                        Console.WriteLine("Wrong command");
                        break;
                }
            }
        }

        public static void Css()
        {
            Console.WriteLine("[return] [show] [add] [drop]");

            while (true)
            {
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.Write("Command: ");
                string command = Console.ReadLine();

                switch (command)
                {
                    case "drop":
                        Drop.DropEntity(Drop.Entity.Css);
                        break;
                    case "return":
                        Console.ResetColor();
                        Console.Clear();
                        return;
                    case "show":
                        Show.ShowEntity(Show.Entity.Css);
                        break;
                    case "add":
                        Add.AddEntity(Add.Entity.Css);
                        break;
                    default:
                        Console.WriteLine("Wrong command");
                        break;
                }
            }
        }

        public static void Sql()
        {
            Console.WriteLine("[return] [show] [add] [drop]");

            while (true)
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.Write("Command: ");
                string command = Console.ReadLine();

                switch (command)
                {
                    case "drop":
                        Drop.DropEntity(Drop.Entity.Sql);
                        break;
                    case "return":
                        Console.Clear();
                        Console.ResetColor();
                        return;
                    case "show":
                        Show.ShowEntity(Show.Entity.Sql);
                        break;
                    case "add":
                        Add.AddEntity(Add.Entity.Sql);
                        break;
                    default:
                        Console.WriteLine("Wrong command");
                        break;
                }
            }
        }
    }
}
