﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using DevHelper;
using DevHelper.Entity;

namespace DevHelperMsTests
{
    [TestClass]
    public class QueriesClassTests
    {
        [TestMethod]
        public void GetGitTest()
        {
            bool result;

            try
            {
                var answer = Queries.GetGit();
                result = true;
            }
            catch (Exception)
            {
                result = false;
            }

            Assert.IsTrue(result);

        }

        [TestMethod]
        public void GetDotnet()
        {
            bool result;

            try
            {
                var answer = Queries.GetDotnet();
                result = true;
            }
            catch (Exception)
            {
                result = false;
            }

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void GetHtml()
        {
            bool result;

            try
            {
                var answer = Queries.GetHtml();
                result = true;
            }
            catch (Exception)
            {
                result = false;
            }

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void GetCss()
        {
            bool result;

            try
            {
                var answer = Queries.GetCss();
                result = true;
            }
            catch (Exception)
            {
                result = false;
            }

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void GetSql()
        {
            bool result;

            try
            {
                var answer = Queries.GetSql();
                result = true;
            }
            catch (Exception)
            {
                result = false;
            }

            Assert.IsTrue(result);
        }
    }
}
