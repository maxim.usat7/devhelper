﻿using DevHelper;
using DevHelper.Entity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace DevHelperMsTests
{
    [TestClass]
    public class LogicClassTests
    {
        [TestMethod]
        public void LanguageTest()
        {
            bool result;

            try
            {
                Logic.Language();
                result = true;
            }
            catch (Exception)
            {
                result = false;
            }

            Assert.IsTrue(result);
        }

    }
}
